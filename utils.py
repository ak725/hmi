import serial.tools.list_ports
import imutils
import config as cf
import cv2
import numpy as np
import time
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class Worker(QRunnable):
    '''
    Worker thread
    '''

    @pyqtSlot()
    def run(self):
        '''
        Your code goes in this function
        '''
        print("Thread start")
        time.sleep(5)
        print("Thread complete")
        
        
class Listening(QRunnable):
    def set_params(self, ser, num_last_datas, split_by, extract_func, name = "Listenning"):
        self.ser = ser
        self.num_last_datas = num_last_datas
        self.split_by = split_by
        self.buffer = ""
        self.name = name
        self.extract_func = extract_func
        
    def  read_last_data(self):
        self.buffer += self.ser.read(self.ser.inWaiting()).decode()
        if self.split_by in self.buffer:
            datas = self.buffer.split(self.split_by)
            x = min(self.num_last_datas, len(datas)-1)
            if x<1:
                return None, 0
            messages = datas[-1-x:-1]
            self.buffer = datas[-1]
            return messages, x
        else:
            return None, 0

    @pyqtSlot()
    def run(self):
        print("Connect:", cf.stt_connect)
        print(self.name+ " Thread started!")
        while cf.RUN:
            time.sleep(0.0001)
            try:
                messages, x = self.read_last_data()
                self.extract_func(messages, x)
            except Exception as e:
                print("Connect:", cf.stt_connect)
                print(self.name + " error:")
                print(e)
        print(self.name+" Thread complete")
        
        
def cotngam_extract_func(messages, x):
    if x>0:
        splits = messages[0].split("-")
        Range, Angle= float(splits[0]), float(splits[1])
        cf.COTNGAM_ENCODERS = [Angle, Range]
#         print(cf.COTNGAM_ENCODERS)
        
def module_2560_extract_func(messages, x):
    if x>0:
        for message in messages:
            header, topic, value = message[:2], message[2], message[3:]
            if header == "CL":
                if topic == "0":
                    cf.POWER_ANGLE = int(value)
                if topic == "1":
                    cf.POWER_RANGE = int(value)
                if topic == "2":
                    cf.CONTROL = int(value)
                if topic == "3":
                    cf.COOLING = int(value)
                if topic == "4":
                    cf.DRIVE = int(value)
                if topic == "5":
                    cf.SHOT_CIRCUT = int(value)
                if topic == "6":
                    cf.LEFT = int(value)
                if topic == "7":
                    cf.RIGHT = int(value)
                if topic == "A":
                    cf.TEM = int(value)
                if topic == "B":
                    cf.PRESS = int(value)
                    
def encoder_tam_extract_func(messages, x):
    if x>0:
        _range = int(messages[-1])
        if _range != cf.RANGE:
            if -15<_range<=60:
                cf.RANGE = _range
            
def encoder_huong_extract_func(messages, x):
    if x>0:
        angle = int(messages[-1])
        if angle != cf.ANGLE:
            if angle>180:
                angle = angle-360
            cf.ANGLE = angle
        
        
def get_list_comps():
    ports_list = []
    ports = serial.tools.list_ports.comports()
    for port, desc, hwid in sorted(ports):
            ports_list.append(port)
    return ports_list
    
def overlay_draw(img0, img2, angle):
    img = img0.copy()
    rotated = imutils.rotate(img2, -angle+180)
    Size = img.shape[0]
    I = img2.shape[0]//2
    rotated = cv2.circle(rotated, (I, I), 300, (255, 255, 255), 220)
    rotated_gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)
    Size2 = rotated.shape[0]
    img[200:600, 200:600][rotated_gray<200] = rotated[rotated_gray<200]
    return img

def overlay_draw2(img0, gun, img2, angle):
    img = img0.copy()
    rotated = imutils.rotate(gun, -angle)
    I = gun.shape[0]//2
    rotated = cv2.circle(rotated, (I, I), 300, (255, 255, 255), 220)[:304, :224]
    def overlay_draw3(img, img2):
        Size = img.shape
        img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        Size2 = img2_gray.shape
        img[Size[0]-Size2[0]:, Size[1]-Size2[1]:][img2_gray<230] = img2[img2_gray<230]
        return img

    img = overlay_draw3(img, rotated)
    img = overlay_draw3(img, img2)
    return img

def overlay_draw_set(img0, img2, angle):
    img = img0.copy()
    rotated = imutils.rotate(img2, -angle+180)
    black_pixels = np.where(
    (rotated[:, :, 0] <= 100) & 
    (rotated[:, :, 1] <= 100) & 
    (rotated[:, :, 2] <= 100)
    )
    rotated[black_pixels] = [255, 255, 255]
    Size = img.shape[0]
    I = img2.shape[0]//2
    rotated = cv2.circle(rotated, (I, I), 500, (255, 255, 255), 220)
    rotated_gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)
    Size2 = rotated.shape[0]
    img[140:660, 140:660][rotated_gray<200] = rotated[rotated_gray<200]
    return img

def overlay_draw2_set(img0, gun, angle):
    img = img0.copy()
    rotated = imutils.rotate(gun, -angle)
    black_pixels = np.where(
    (rotated[:, :, 0] <= 100) & 
    (rotated[:, :, 1] <= 100) & 
    (rotated[:, :, 2] <= 100)
    )
    rotated[black_pixels] = [255, 255, 255]
    I = gun.shape[0]//2
    rotated = cv2.circle(rotated, (I, I), 500, (255, 255, 255), 220)[:364, :285]
    def overlay_draw3(img, img2):
        Size = img.shape
        img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        Size2 = img2_gray.shape
        img[Size[0]-Size2[0]:, Size[1]-Size2[1]:][img2_gray<230] = img2[img2_gray<230]
        return img

    img = overlay_draw3(img, rotated)
    return img




            
