on_color = "rgb(59, 179, 114)"
off_color = "rgb(254, 78, 78)"
button_on_style = '''
QPushButton{
	border-width: 1px;
	color: rgb(255, 255, 255);
	background-color: rgb(59, 179, 114);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:hover{
	border: 1px;
	color: rgb(255, 255, 255);
	background-color: rgb(40, 185, 100);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:pressed{
	border: 3px;
	color: rgb(255, 255, 255);
	background-color: rgb(40, 185, 100);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: inset;
}'''

button_off_style = '''
QPushButton{
	border-width: 1px;
	color: rgb(255, 255, 255);
	background-color: rgb(254, 78, 78);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:hover{
	border: 1px;
	color: rgb(255, 255, 255);
	background-color: rgb(254, 60, 60);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:pressed{
	border: 3px;
	color: rgb(255, 255, 255);
	background-color: rgb(254, 78, 78);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: inset;
}
'''

button_neural_style = '''
QPushButton{
	border-width: 1px;
	color: rgb(255, 255, 255);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:hover{
	border: 1px;
	color: rgb(255, 255, 255);
	background-color: rgb(100, 100, 100);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: outset;
}

QPushButton:pressed{
	border: 3px;
	color: rgb(255, 255, 255);
	border-radius: 5px;
	border-color:rgb(40, 40, 40);
	border-style: inset;
}
'''

