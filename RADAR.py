
# Project: Dự án HĐH AK725
# Author: Vũ Nguyễn Minh Hùng
# Date: 2022-04-21
# File: RADAR enc.py
# Description: Ket noi encoder tam, huong o che do radar
# Version: 1.0

from concurrent.futures import thread
import serial
import time
import config as cf
import threading

port_ser_huong = "COM3"
port_ser_tam = "COM4"
baud = 38400

cf.RADAR_HUONG = 180.0
cf.RADAR_TAM = 0.0
cf.byteArray_1 = []
cf.byteArray_2 = []

cf.ser_radar_huong = serial.Serial()
cf.ser_radar_tam = serial.Serial()

def print_log(msg):
    pass
    if __name__ == '__main__':
        print('+ Radar: ' + msg)

def openSerialPort(port_ser_huong, port_ser_tam, baud):
    try:
        cf.ser_radar_huong = serial.Serial(
            port = port_ser_huong, 
            baudrate = baud,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS)
        cf.ser_radar_tam = serial.Serial(
            port = port_ser_tam, 
            baudrate = baud,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS)
        print_log("+ Radar connected.")   
    except Exception as e:
        print_log("+ Radar connect error: " + str(e))

def sendCommand():
    try:
        cf.ser_radar_huong.write([0x01, 0x80, 0x02, 0x80, 0x04])
        cf.ser_radar_tam.write([0x01, 0x80, 0x02, 0x80, 0x04])
    except Exception as e:
        print_log('+ Radar send error: ' + str(e))

def readRadarValue():
    try:
        # Try to open serial port:
        # if(cf.ser_radar_huong.isOpen() == False or cf.ser_radar_tam.isOpen() == False):
        #     cf.ser_radar_huong.close()
        #     cf.ser_radar_tam.close()
        #     global port_ser_huong, port_ser_tam, baud
        #     openSerialPort(port_ser_huong, port_ser_tam, baud)
        #     return
        while(cf.ser_radar_huong.isOpen() and cf.ser_radar_huong.in_waiting > 0):
            _new = cf.ser_radar_huong.read()
            if _new is not None:
                cf.byteArray_1.extend(_new)
            if(_new == b'\x04' and len(cf.byteArray_1) >= 8):
                list_int = [int(x) for x in cf.byteArray_1]
                try:
                    # Check sum:
                    if (list_int[0] == int(0x01) and list_int[7] == int(0x04) and
                        list_int[6] == (list_int[1] ^ list_int[2] ^ list_int[3] ^ list_int[4] ^ list_int[5])): 
                        _radar_huong = (list_int[4] * 256.0 + list_int[5]) * 360.0 / 8192.0
                        if _radar_huong >= 360.0:
                            _radar_huong = _radar_huong - 360.0
                        if _radar_huong >= 180.0:
                            _radar_huong = 180 + (180 - _radar_huong )
                        elif _radar_huong < 180.0:
                            _radar_huong = 180 + (180 - _radar_huong )
                        cf.RADAR_HUONG = round(_radar_huong,1)
                    else:
                        print_log('--> radar huong Check xor error.')
                except: 
                    pass
                cf.byteArray_1 = bytearray(b'')
        while(cf.ser_radar_tam.isOpen() and cf.ser_radar_tam.in_waiting > 0):
            _new = cf.ser_radar_tam.read()
            if _new is not None:
                cf.byteArray_2.extend(_new)
            if(_new == b'\x04' and len(cf.byteArray_2) >= 8):
                list_int = [int(x) for x in cf.byteArray_2]
                try:
                    # Check sum:
                    if (list_int[0] == int(0x01) and list_int[7] == int(0x04) and
                        list_int[6] == (list_int[1] ^ list_int[2] ^ list_int[3] ^ list_int[4] ^ list_int[5])): 
                        _radar_tam = (list_int[4] * 256.0 + list_int[5]) * 360.0 / 8192.0
                        if _radar_tam >= 360.0:
                            _radar_tam = _radar_tam - 360.0
                        cf.RADAR_TAM = 360.0 - _radar_tam # 19/5/2022 - Hùng sửa để test thử enc radar
                        cf.RADAR_TAM = round(cf.RADAR_TAM,1)
                    else:
                        print_log('--> radar tam Check xor error.')
                except: 
                    pass
                cf.byteArray_2 = bytearray(b'')
    except Exception as _ee:
        print_log("+ Radar read error: " + str(_ee))

def updateRADAR():
    while True:
        time.sleep(0.005)
        sendCommand()
        readRadarValue()
        print_log('Hướng ' + str(cf.RADAR_HUONG) + '; Tầm ' + str(cf.RADAR_TAM))

t1 = threading.Thread(target = updateRADAR, daemon=False).start()

if __name__ == '__main__': 
    openSerialPort(port_ser_huong, port_ser_tam, baud)