#!/usr/bin/env python
# coding: utf-8
from email.policy import strict
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtCore, QtGui, QtWidgets, uic

import pymodbus
from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient #initialize a serial RTU client instance
from pymodbus.transaction import ModbusRtuFramer

from functools import partial
import config as cf
import cv2
import serial 
from styleSheet import *
import threading
import imutils
#from matplotlib import pyplot as plt
import time
import datetime
from utils import *
import os

# Hung - import script to connect to radar sys
import RADAR

root_path = os.path.dirname(os.path.realpath(__file__)) + '/'
# root_path = "C:/HDH725/AK725/"

pyQTfileName = root_path+ "QT5/main.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(pyQTfileName)    

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)
QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)

cf.is_permission_log = True
def write_csv_log(data):
    global root_path
    if cf.is_permission_log == False:
        return
    try:
        file_name = root_path + 'Logs/' + str(datetime.datetime.now().strftime("%Y-%m-%d")) + ' tam.csv'
        with open(file_name, 'a', newline='') as csvfile: 
            csvfile.write(data)
            csvfile.write('\n')
            # print ('Logged')
    except Exception as e:
        pass
        cf.is_permission_log = False

write_csv_log("\n\n\n----- [" + str(datetime.datetime.now()) + "] : Started ----\n")
write_csv_log("Time,Set_Angle,Act_Angle,U_dk,Speed,Error")

# save_log()

def update_system():
    t = time.localtime()
    day = datetime.date.today()
    day_str = day.strftime("%d/%m/%y")
    current_time = time.strftime("%H:%M:%S", t)
    window.current_time.setText(day_str+"  "+ current_time)
    update_power_status()
    update_control_status()
    update_cooling_status()
    update_angle_status()
    update_range_status()
    update_tem_press()
    update_cot_ngam_va_radar()
    send_data_to_cot_ngam()
    update_mach_ban()
    update_so_luong_dan_con_lai()
    update_dan_dong()
        
class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.threadpool = QThreadPool()
        print("+ Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
        self.qTimer = QTimer()
        # set interval to 1 s
        self.qTimer.setInterval(10) # 1000 ms = 1 s
        # connect timeout signal to signal handler
        self.qTimer.timeout.connect(update_system)
        # start timer
        self.qTimer.start()
        
def oh_no():
    worker = Worker()
    window.threadpool.start(worker)


app = QtWidgets.QApplication(sys.argv)
window = MyApp()

on_status_pixmap = QPixmap(root_path+"Images/green.png")
off_status_pixmap = QPixmap(root_path+"Images/red.png")
black_status_pixmap = QPixmap(root_path+"Images/black.png")

num_serial = 6
cf.buffer_strings = ["" for i in range(num_serial)]

cf.RUN = False
cf.POWER_ANGLE = False
cf.POWER_RANGE = False
cf.CONTROL = 0
cf.COOLING = False
cf.ANGLE = 180
cf.RANGE = 45
cf.TEMs = np.ones(4, np.int8)*30
cf.PRESSes = np.ones(4, np.int8)*30
cf.CURRENT_ANGLE_COMMAND = 180.0
cf.CURRENT_RANGE_COMMAND = 0
cf.COTNGAM_ENCODERS = [180.0, 0.0]
cf.RADAR_HUONG = 180.0 # Hung
cf.RADAR_TAM = 0.0 # Hung
cf.DAN_TRAI = 0
cf.DAN_PHAI = 0

cf.NGUON_SAR = False
cf.DAN_DONG_HUONG = False
cf.DAN_DONG_TAM = False
cf.DAN_TRAI_GIAM = 0
cf.DAN_PHAI_GIAM = 0

cf.CONTROL_old = 0
cf.MACH_BAN_ENABLE = False
cf.MACH_BAN_ENABLE_old = False
cf.CHECK_MACH_BAN = False
cf.VUNG_BAN = False
cf.DONG_BO = False
cf.SAI_SO_CHO_PHEP = 0.72

cf.listening_2560 = False
cf.listening_Encoder_huong = False
cf.listening_Encoder_tam = False
cf.listening_cotngam = False
cf.connected_time = None

cf.ananlog_connection = False


img_gun_derection_bg = cv2.imread(root_path+"Images/1.png")
img_gun_derection = cv2.imread(root_path+"Images/2.png")
img_gun_derection = cv2.resize(img_gun_derection, (400, 400))

img_gun_range_bg = cv2.imread(root_path+"Images/3.png")
img_gun_range = cv2.imread(root_path+"Images/gun.jpg")
img_gun_base = cv2.imread(root_path+"Images/4.png")

set_angle_img = cv2.imread(root_path+"Images/set_angle.png")
set_angle_img = cv2.resize(set_angle_img, (520, 520))
set_range_img = cv2.imread(root_path+"Images/set_range.png")
set_range_img = cv2.resize(set_range_img, (520, 520))


def update_cot_ngam_va_radar():
    if cf.CONTROL == 1: # cot ngam
        _huong = cf.COTNGAM_ENCODERS[0]
        _tam = cf.COTNGAM_ENCODERS[1]
        if(_huong > 180):
            _huong -= 360.0
        if(_tam > 180):
            _tam -= 360.0
        _huong = round(_huong,2)
        _tam = round(_tam,2)
        window.angle_set.setText(str(_huong))
        window.range_set.setText(str(_tam))
        click_angle_range_btn()
    elif cf.CONTROL == 2: # radar # Hung
        _huong = cf.RADAR_HUONG
        _tam = cf.RADAR_TAM
        if(_huong > 180):
            _huong -= 360.0
        if(_tam > 180):
            _tam -= 360.0
        _huong = round(_huong,2)
        _tam = round(_tam,2)
        window.angle_set.setText(str(_huong))
        window.range_set.setText(str(_tam))
        click_angle_range_btn()

cf.last_time_send_cot_ngam = time.time()
def send_data_to_cot_ngam():
    _new_time = time.time()
    if _new_time - cf.last_time_send_cot_ngam >= 0.2:
        # print(_new_time)
        cf.last_time_send_cot_ngam = _new_time
        try: 
            if(cf.connect_statuses[3] == True):
                # Thong tin so luong dan:
                _cmd_cot_ngam = 'DA' + 'T:' + str(cf.DAN_TRAI) + ', P:' + str(cf.DAN_PHAI) + '#'
                cf.serials[3].write(_cmd_cot_ngam.encode())
                # Thong tin huong phao:
                _cmd_cot_ngam = 'HU' + str(cf.ANGLE) + '#'
                cf.serials[3].write(_cmd_cot_ngam.encode())
                # Thong tin tam phao:
                _cmd_cot_ngam = 'TA' + str(cf.RANGE) + '#'
                cf.serials[3].write(_cmd_cot_ngam.encode())
                # Hien thi den led tren cot ngam:
                _cmd_cot_ngam = 'PC'
                _cmd_cot_ngam += '1' if cf.CONTROL == 1 else '0' # che do cot ngam
                _cmd_cot_ngam += '1' if cf.COOLING == True else '0' # lam mat
                _cmd_cot_ngam += '01' if cf.VUNG_BAN == True else '10' # vung nguy hiem / vung ban
                _cmd_cot_ngam += '01' if cf.MACH_BAN_ENABLE else '10' # khong dong bo / mach ban san sang
                _cmd_cot_ngam += '#'               
                cf.serials[3].write(_cmd_cot_ngam.encode())
        except:
            pass

def update_so_luong_dan_con_lai():
    if (cf.DAN_TRAI_GIAM > 0 and cf.DAN_TRAI - cf.DAN_TRAI_GIAM >= 0):
        cf.DAN_TRAI -= cf.DAN_TRAI_GIAM
        cf.DAN_TRAI_GIAM = 0
        window.dan_trai.setText(str(cf.DAN_TRAI))
    if (cf.DAN_PHAI_GIAM > 0 and cf.DAN_PHAI - cf.DAN_PHAI_GIAM >= 0):
        cf.DAN_PHAI -= cf.DAN_PHAI_GIAM
        cf.DAN_PHAI_GIAM = 0
        window.dan_phai.setText(str(cf.DAN_PHAI))

def setup_so_luong_dan_con_lai():
    try:
        # sys.exit(0)
        cf.DAN_TRAI = int(window.dan_trai.text())
        cf.DAN_PHAI = int(window.dan_phai.text())
        cf.DAN_TRAI_GIAM = 0
        cf.DAN_PHAI_GIAM = 0
    except Exception as e:
        print(e)
        pass

def update_dan_dong():
    if (cf.RUN == False):
        window.led_SAR.setPixmap(black_status_pixmap)
        window.led_dan_dong_huong.setPixmap(black_status_pixmap)
        window.led_dan_dong_tam.setPixmap(black_status_pixmap)
        return
    else: 
        set_led_status(window.led_SAR, cf.NGUON_SAR)    
        set_led_status(window.led_dan_dong_huong, cf.DAN_DONG_HUONG) 
        set_led_status(window.led_dan_dong_tam, cf.DAN_DONG_TAM) 


def update_mach_ban():
    if (cf.RUN == False):
        window.led_vung_nguy_hiem.setPixmap(black_status_pixmap)
        window.led_vung_ban.setPixmap(black_status_pixmap)
        window.led_khong_dong_bo.setPixmap(black_status_pixmap)
        window.led_dong_bo.setPixmap(black_status_pixmap)
        window.led_san_sang_ban.setPixmap(black_status_pixmap)
        window.led_check_mach_ban.setPixmap(black_status_pixmap)
        window.led_dan_dong_tam.setPixmap(black_status_pixmap)
        window.led_dan_dong_huong.setPixmap(black_status_pixmap)
        cf.MACH_BAN_ENABLE_old = False
        cf.MACH_BAN_ENABLE  = False
        cf.CONTROL_old = 0
        cf.CONTROL = 0
        return
    
    set_led_status(window.led_check_mach_ban, cf.CHECK_MACH_BAN)

    cf.VUNG_BAN = (abs(cf.ANGLE) >= 35 and 0 <= cf.RANGE <= 75)
    set_led_status(window.led_vung_ban, cf.VUNG_BAN)
    set_led_status(window.led_vung_nguy_hiem, cf.VUNG_BAN == False)

    error = cf.ANGLE - cf.CURRENT_ANGLE_COMMAND
    error0 = error
    if error0>180:
        error = 360 - error0
    elif error0<-180:
        error = 360 + error0

    # sai số cho phép
    cf.DONG_BO = (abs(error) <= cf.SAI_SO_CHO_PHEP and 
        abs(cf.RANGE - cf.CURRENT_RANGE_COMMAND) <= cf.SAI_SO_CHO_PHEP and
        cf.CURRENT_RANGE_COMMAND != -999 and cf.CURRENT_ANGLE_COMMAND != -1)
    set_led_status(window.led_khong_dong_bo, cf.DONG_BO == False)
    set_led_status(window.led_dong_bo, cf.DONG_BO)

    if cf.DONG_BO and cf.VUNG_BAN: # and cf.DAN_DONG_HUONG and cf.DAN_DONG_TAM: 
        cf.MACH_BAN_ENABLE = True
    else:
        cf.MACH_BAN_ENABLE = False
    set_led_status(window.led_san_sang_ban, cf.MACH_BAN_ENABLE)

    if cf.MACH_BAN_ENABLE == True and cf.MACH_BAN_ENABLE_old == False:
        on_off_btn_click(1, 5)
        cf.MACH_BAN_ENABLE_old = cf.MACH_BAN_ENABLE
    elif cf.MACH_BAN_ENABLE == False and cf.MACH_BAN_ENABLE_old == True:
        on_off_btn_click(0, 5)
        cf.MACH_BAN_ENABLE_old = cf.MACH_BAN_ENABLE
    
    if cf.CONTROL == 1 and cf.CONTROL_old != 1: # chế độ cột ngắm
        on_off_btn_click(1, 4)
        cf.CONTROL_old = cf.CONTROL
    elif cf.CONTROL != 1 and cf.CONTROL_old == 1:
        on_off_btn_click(0, 4)
        cf.CONTROL_old = cf.CONTROL 

def update_angle_status():
    if cf.connect_statuses[0] == True:
        image = overlay_draw(img_gun_derection_bg, img_gun_derection,cf.ANGLE)
        if cf.CURRENT_ANGLE_COMMAND != -1:
            image = overlay_draw_set(image, set_angle_img, cf.CURRENT_ANGLE_COMMAND)
    else:
        image = img_gun_derection_bg
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = QtGui.QImage(image, image.shape[1], image.shape[0], image.shape[1] * 3, QtGui.QImage.Format_RGB888)
    pix = QtGui.QPixmap(image)
    window.img_direction.setPixmap(pix)
    window.text_angle.setText(str(round(cf.ANGLE,2)))
    
    error = 180
    # print("b2560:")
    # print(cf.connect_statuses[0])
    if cf.CURRENT_ANGLE_COMMAND == -1:
        window.angle_error.setText('Góc giới hạn')
    else:
        error = cf.ANGLE - cf.CURRENT_ANGLE_COMMAND
        error0 = error
        if error0 > 180:
            error = 360 - error0
        elif error0 < -180:
            error = 360 + error0
        window.angle_error.setText(str(round(error,2)))
       
def update_range_status():
    if cf.connect_statuses[0] == True:
        image = overlay_draw2(img_gun_range_bg, img_gun_range, img_gun_base, cf.RANGE)
        if cf.CURRENT_RANGE_COMMAND != -999:
            image = overlay_draw2_set(image, set_range_img, cf.CURRENT_RANGE_COMMAND)
    else:
        image = img_gun_range_bg
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = QtGui.QImage(image, image.shape[1], image.shape[0], image.shape[1] * 3, QtGui.QImage.Format_RGB888)
    pix = QtGui.QPixmap(image)
    window.img_range.setPixmap(pix)
    window.text_range.setText(str(round(cf.RANGE, 2)))
    if cf.CURRENT_RANGE_COMMAND == -999:
        window.range_error.setText('Góc giới hạn')
    else:
        error = cf.RANGE - cf.CURRENT_RANGE_COMMAND
        window.range_error.setText(str(round(error, 2)))
    
def set_led_status(led, status, text_label = None, text = None):
    if status:
        led.setPixmap(on_status_pixmap)
    else:
        led.setPixmap(off_status_pixmap)
    if text_label is not None:
        text_label.setText(text)
        
def set_button_status(button, status, is_on_btn):
    if status:
        if is_on_btn:
            button.setStyleSheet(button_on_style)
        else:
            button.setStyleSheet(button_off_style)
    else:
        button.setStyleSheet(button_neural_style)
    
def update_power_status( ):

    set_button_status(window.power_angle_on_btn, cf.POWER_ANGLE, True)
    set_button_status(window.power_angle_off_btn, not cf.POWER_ANGLE, False)
    
    set_button_status(window.power_range_on_btn, cf.POWER_RANGE, True)
    set_button_status(window.power_range_off_btn, not cf.POWER_RANGE, False)

    
def update_control_status():
    set_button_status(window.control_rada_btn, False, True)
    set_button_status(window.control_cotngam_btn, False, True)
    set_button_status(window.control_thucong_btn, False, True)
    if cf.CONTROL == 2:
        text = "RA ĐA"
        set_button_status(window.control_rada_btn, True, True)
        window.angle_range_set_btn.setText('')
    if cf.CONTROL == 1:
        text = "CỘT NGẮM"
        set_button_status(window.control_cotngam_btn, True, True)
        window.angle_range_set_btn.setText('')
    if cf.CONTROL == 0:
        text = "ĐK TẠI CHỖ"
        set_button_status(window.control_thucong_btn, True, True)
        window.angle_range_set_btn.setText('ÁP DỤNG')
        
    set_led_status(window.control_led, cf.CONTROL, window.control_label, text)

    
def update_cooling_status():
    if cf.RUN == False:
        window.led_lam_mat.setPixmap(black_status_pixmap)
    else:
        set_led_status(window.led_lam_mat, cf.COOLING)

def on_off_btn_click(_value, _topic):
    try:
        _command = "PC"+str(_topic)+str(_value)+"#"
        cf.serials[0].write(_command.encode())
        print(_command)
    except Exception as e:
        print("on_off_btn_click error:")
        print(e)

def update_tem_press():
    if cf.ananlog_connection:
        try: 
            analog_read= cf.analog_client.read_input_registers(0x00,8,unit= 0x03)
        
            for i in range(2):
                data_temp = analog_read.registers[i]
                cf.TEMs[i] = data_temp /65535.0 * 125.0 - 15.0
                data_temp2 = analog_read.registers[i+4]
                cf.TEMs[i+2] = data_temp2 /65535.0 * 125.0 - 15.0

                data_pressure = analog_read.registers[2+i]
                cf.PRESSes[i] = data_pressure/65535.0 * 250.0 + 6.0
                data_pressure2 = analog_read.registers[6+i]
                cf.PRESSes[i+2] = data_pressure2/65535.0 * 250.0 + 6.0

                # print ('t1 = ',data_temp,', p1 = ', data_pressure)

            window.Tem_text.setText(str(round(cf.TEMs[0],2)))
            window.Press_text.setText(str(round(cf.PRESSes[0],2)))
            window.Tem_text_2.setText(str(round(cf.TEMs[1],2)))
            window.Press_text_2.setText(str(round(cf.PRESSes[1],2)))
            window.Tem_text_3.setText(str(round(cf.TEMs[2],2)))
            window.Press_text_3.setText(str(round(cf.PRESSes[2],2)))
            window.Tem_text_4.setText(str(round(cf.TEMs[3],2)))
            window.Press_text_4.setText(str(round(cf.PRESSes[3],2)))

        except Exception as e:
            cf.ananlog_connection = False # Hung
            cf.DAN_DONG_HUONG = False
            cf.DAN_DONG_TAM = False
            cf.NGUON_SAR = False
            cf.MACH_BAN_ENABLE = False
            cf.MACH_BAN_ENABLE_old = False
            cf.CHECK_MACH_BAN = False
            print("Read ADAM error: " + str(e))
            print("Connect to ADAM device: ", cf.ananlog_connection)

def swich_control_mode(value, topic):
    cf.CONTROL = value        

def click_angle_range_btn():
    try:
        angle = window.angle_set.text()
        angle = 0 if angle == "" else float(angle)
        _range = window.range_set.text()
        _range = 0 if _range == "" else float(_range)
        angle_range_command(angle, _range)
    except Exception as e:
        print('Invalid Angle or Range - Syntax Error')

cf.last_time_angle_range= time.time()
def angle_range_command(angle, _range):
    if time.time() - cf.last_time_angle_range > 0.001:
        cf.last_time_angle_range = time.time()
        try:
            # set angle
            angle_int = int(angle*10)   
            if angle_int<0:
                angle_int +=3600
            if 320<=angle_int<=3600-320: # vung an toan # Hung
                cf.CURRENT_ANGLE_COMMAND = round(angle_int/10, 2)
                if 100<= angle_int<1000:
                    angle_int = "0"+str(angle_int) 
                elif 10<=angle_int <100:
                    angle_int = "00"+str(angle_int)
                elif angle_int<10:
                    angle_int = "000"+str(angle_int)
                #print("CURRENT_ANGLE_COMMAND", cf.CURRENT_ANGLE_COMMAND)
                #angle = str(angle)+"0"
                command = "p"+ str(angle_int)
                # print("set angle")
                # print(command + ' -> time: ' + str(cf.last_time_angle_range))
                if cf.DAN_DONG_HUONG == True:
                    cf.serials[1].write(command.encode())
            else:
                cf.CURRENT_ANGLE_COMMAND = -1
        except Exception as e:
            print("angle_range_command faild:")
            print(e)

        try: 
            # set range
            _range_int = int(_range*10)
            if -30<=_range_int<=820: # vung an toan # Hung
                cf.CURRENT_RANGE_COMMAND = round(_range_int/10, 2)
                if _range_int<0:
                    _range_int = 3600+_range_int
                #print("CURRENT_RANGE_COMMAND", cf.CURRENT_RANGE_COMMAND)
                if 1000>_range_int>=100:
                    _range_int = "0"+str(_range_int)
                elif 10<= _range_int<100:
                    _range_int = "00"+str(_range_int)
                elif _range_int < 10:
                    _range_int = "000"+str(_range_int)
                command = "p"+ str(_range_int)
                # print("set range")
                # print(command)
                if cf.DAN_DONG_TAM == True:
                    cf.serials[2].write(command.encode())
            else:
                cf.CURRENT_RANGE_COMMAND = -999
        except Exception as e:
            print("angle_range_command faild:")
            print(e)

def click_author():
    global window
    _author = 'Bộ môn Kỹ thuật điện - Khoa Kỹ thuật điều khiển\nHọc viện Kỹ thuật Quân sự\nHà Nội - 2021'
    QMessageBox.about(window, 'About', _author)
    print('\n+ Author:\n' + _author)

class Listening(QRunnable):
    def set_params(self, ser, num_last_datas, split_by, extract_func, name = "Listenning"):
        self.ser = ser
        self.num_last_datas = num_last_datas
        self.split_by = split_by
        self.buffer = ""
        self.name = name
        self.extract_func = extract_func
        
    def  read_last_data(self):
        self.buffer += self.ser.read(self.ser.inWaiting()).decode()
        if self.split_by in self.buffer:
            datas = self.buffer.split(self.split_by)
            x = min(self.num_last_datas, len(datas)-1)
            if x<1:
                return None, 0
            messages = datas[-1-x:-1]
            self.buffer = datas[-1]
            return messages, x
        else:
            return None, 0

    @pyqtSlot()
    def run(self):
        print("Connect:", cf.stt_connect)
        print(self.name+ " Thread started!")
        while cf.RUN:
            time.sleep(0.0001)
            try:
                messages, x = self.read_last_data()
                self.extract_func(messages, x)
            except Exception as e:
                print("Connect:", cf.stt_connect)
                print(self.name + " error:")
                print(e)
        print(self.name+" Thread complete")
        
        
def cotngam_extract_func(messages, x):
    if x>0:
        splits = messages[0].split("-")
        # print(messages)
        try:
            Range, Angle, Lazer, Sum = float(splits[0]), float(splits[1]), float(splits[2]), float(splits[3])
            #print(Range, Angle, Lazer)
            if (Sum == Range + Angle + Lazer):
                if Angle >= 180.0:
                    Angle -= 360.0
                if Range >= 180.0:
                    Range -= 360.0
                cf.COTNGAM_ENCODERS = [Angle, Range]
                # print(cf.COTNGAM_ENCODERS)
        except:
            pass
        
def module_2560_extract_func(messages, x):
    if x>0:
        for message in messages:
            topic = []
            header, topic, value = message[:2], message[2:10], message[10:]
            if header == "CL": 
                cf.NGUON_SAR = True
                cf.POWER_ANGLE = int(topic[1]) # báo bật nguồn hướng
                cf.POWER_RANGE = int(topic[0])  # báo bật nguồn tầm
                cf.DAN_DONG_HUONG = bool(int(topic[2]))
                cf.DAN_DONG_TAM = bool(int(topic[3]))
                cf.DAN_TRAI_GIAM = int(topic[4])
                cf.DAN_PHAI_GIAM = int(topic[5])
                cf.COOLING = bool(int(topic[6]))
                cf.CHECK_MACH_BAN = bool(int(topic[7]))
                # print(' + Trạng thái mạch bắn: ' + str(cf.CHECK_MACH_BAN))
               
def encoder_tam_extract_func(messages, x):
    try:
        if x>0:
            _range = int(messages[-1][:4])/10
            Act_Ang = _range
            if _range != cf.RANGE:
                if _range > 180: # Hung
                    _range = _range - 360.0
                if -30<_range<90:
                    cf.RANGE = _range

            print('+ Data = ' + messages[-1][4:])
            time_now = datetime.datetime.now().strftime("%H:%M:%S")
            split_messages = str(messages[-1][4:]).split('/')
            Set_Ang = cf.CURRENT_RANGE_COMMAND
            Udk = int(str(split_messages[0]).strip(' \t\r\n\0').replace('N','-'))
            Speed = int(str(split_messages[1]).strip(' \t\r\n\0').replace('N','-'))
            comma = ","
            data = str(time_now) +  comma + str(Set_Ang) + comma + str(Act_Ang) + comma + str(Udk) + comma + str(Speed) +  comma + str(10*(Set_Ang-Act_Ang))
            if(Speed != 0 and abs(Speed)<4000 and abs(Udk)<4000):
                write_csv_log(data) # write to csv file
    except:
        pass

cf.old_test_counter = 0
cf.time_old_test_counter = time.time() 
def encoder_huong_extract_func(messages, x):
    try:
        if x>0:
            angle = int(messages[-1][:4])/10    
            # Act_Ang = angle
            if angle != cf.ANGLE:
                if angle>180:
                    angle = angle-360
                cf.ANGLE = angle

            # print('+ Data = ' + messages[-1][4:])
            # time_now = datetime.datetime.now().strftime("%H:%M:%S")
            # split_messages = str(messages[-1][4:]).split('/')
            # Set_Ang = cf.CURRENT_ANGLE_COMMAND
            # Udk = int(str(split_messages[0]).strip(' \t\r\n\0').replace('N','-'))
            # Speed = int(str(split_messages[1]).strip(' \t\r\n\0').replace('N','-'))
            # comma = ","
            # data = str(time_now) +  comma + str(Set_Ang) + comma + str(Act_Ang) + comma + str(Udk) + comma + str(Speed) +  comma + str(10*(Set_Ang-Act_Ang))
            # if(Speed != 0 and abs(Speed)<4000 and abs(Udk)<4000):
            #     pass
                # write_csv_log(data) # write to csv file
            # print('+ Data = ' + data)
            
            # calculate sample time of control circuit 's loop:
            # _new_time = time.time()
            # _test_counter = int(messages[-1][4:].replace(' ',''))
            # if (_test_counter - cf.old_test_counter == 0):
            #     return
            # _sample_time = (_new_time - cf.time_old_test_counter)/(_test_counter - cf.old_test_counter)
            # _sample_time = round(_sample_time,5)
            # print('+ sample time (s) = ' + str(_sample_time))
            # cf.time_old_test_counter = _new_time
            # cf.old_test_counter = _test_counter
    except Exception as e:
        print (e)
        pass

def set_new_error():
    try:
        _new_error = float(window.new_error.text())
        if 0 <= _new_error <= 5:
            cf.SAI_SO_CHO_PHEP = _new_error
            _ly_giac = int(_new_error * 6000 / 360)
            window.label_sai_so_dong_bo.setText('(Tương đương ' + str(_ly_giac) + ' ly giác)')
        else:
            window.label_sai_so_dong_bo.setText('(Vượt quá giá trị cho phép)')
    except:
        window.label_sai_so_dong_bo.setText('(Giá trị không hợp lệ)')

window.new_error.setText(str(cf.SAI_SO_CHO_PHEP))

window.power_angle_on_btn.clicked.connect(partial(on_off_btn_click, 1, 0))
window.power_angle_off_btn.clicked.connect(partial(on_off_btn_click, 0, 0))

window.power_range_on_btn.clicked.connect(partial(on_off_btn_click, 1, 1))
window.power_range_off_btn.clicked.connect(partial(on_off_btn_click, 0, 1))

window.control_rada_btn.clicked.connect(partial(swich_control_mode, 2, 2))
window.control_cotngam_btn.clicked.connect(partial(swich_control_mode, 1, 2))
window.control_thucong_btn.clicked.connect(partial(swich_control_mode, 0, 2))

window.angle_range_set_btn.clicked.connect(click_angle_range_btn)
# window.label_HVKTQS.clicked.connect(click_author)

window.btn_ap_dung_dan.clicked.connect(setup_so_luong_dan_con_lai)
window.btn_set_new_error.clicked.connect(set_new_error)


cf.comp_box_names = ["comp"+str(i) for i in range(num_serial)]
cf.com_port = 'COM8', 'COM11', 'COM18', 'COM1', 'COM3', 'COM4'
cf.baud_rate = 19200, 9600, 9600, 9600, 38400, 38400
cf.comp_leds = [window.findChild(QtWidgets.QLabel, "led_"+box_name) for box_name in cf.comp_box_names]
cf.serials = [None for i in range(num_serial)]

cf.connect_statuses = [False for i in range(num_serial)]

cf.stt_connect = 0
class COMConnect(QRunnable):
    @pyqtSlot()
    def run(self):
        ports_history = {}
        cf.connect_statuses = [False for i in range(num_serial)]
        cf.listening_2560 = False
        cf.listening_Encoder_huong  = False
        cf.listening_Encoder_tam = False
        cf.listening_cotngam = False
        cf.listening_RADAR = False

        cf.RUN = False

        for i in range(0,6):
            cf.comp_leds[i].setPixmap(off_status_pixmap)
            try:
                cf.serials[i].flushInput()
                cf.serials[i].flushOutput()
                cf.serials[i].close()
            except:
                pass

            try:
                try:
                    if i== 4 and cf.ser_radar_huong.isOpen():
                        cf.ser_radar_huong.close()
                    if i== 5 and cf.ser_radar_tam.isOpen():
                        cf.ser_radar_tam.close()
                except:
                    pass
                cf.serials[i] = serial.Serial(cf.com_port[i], cf.baud_rate[i])
                cf.comp_leds[i].setPixmap(on_status_pixmap)
                cf.connect_statuses[i] = True
            except Exception as e:
                print("Connect error:")
                print(e)                

        time.sleep(1.0)
        cf.RUN = True

        if not cf.listening_2560 and cf.connect_statuses[0]:
            module_2560_listen_thread = Listening()
            module_2560_listen_thread.set_params(cf.serials[0], 11, "\n", module_2560_extract_func, name = "MODULE 2560")
            window.threadpool.start(module_2560_listen_thread)

        if not cf.listening_Encoder_huong and cf.connect_statuses[1]:
            encoder_huong_listen_thread = Listening()
            encoder_huong_listen_thread.set_params(cf.serials[1], 1, "-", encoder_huong_extract_func, name = "ENCODER HUONG")
            window.threadpool.start(encoder_huong_listen_thread)

        if not cf.listening_Encoder_tam and cf.connect_statuses[2]:
            encoder_tam_listen_thread = Listening()
            encoder_tam_listen_thread.set_params(cf.serials[2], 1, "-", encoder_tam_extract_func, name = "ENCODER TAM")
            window.threadpool.start(encoder_tam_listen_thread)

        if not cf.listening_cotngam and cf.connect_statuses[3]:
            cotngam_listen_thread = Listening()
            cotngam_listen_thread.set_params(cf.serials[3], 1, "\n", cotngam_extract_func, name = "COT NGAM")
            window.threadpool.start(cotngam_listen_thread)
            
        if not cf.listening_RADAR and cf.connect_statuses[4] and cf.connect_statuses[5]:
            port_ser_huong = cf.serials[4].port
            port_ser_tam = cf.serials[5].port
            baud = 38400
            cf.serials[4].close()
            cf.serials[5].close()
            RADAR.openSerialPort(port_ser_huong, port_ser_tam, baud)
            cf.listening_RADAR = True
            
        cf.connected_time = time.time()


def connect_comp_ports():
    cf.stt_connect +=1
    comp_connect = COMConnect()
    window.threadpool.start(comp_connect)

    #Connect to the ADAM via serial modbus server # Hung
    if cf.ananlog_connection == False:
        cf.analog_client = ModbusClient(method='rtu', port='COM2', baudrate=9600, timeout=0.3)
        cf.ananlog_connection = False
        try:
            cf.ananlog_connection = cf.analog_client.connect()
            print("Connect to ADAM device: ", cf.ananlog_connection)
        except:
            pass
    
window.connect_COMP_btn.clicked.connect(connect_comp_ports)

display_monitor = 0
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
window.show()
sys.exit(app.exec_())
# client.close()
exit